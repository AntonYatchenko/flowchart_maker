#coding:utf-8
import pygame

class Blocks:
    def __init__(self):
        self.and_block_true=pygame.image.load('blocks/and_block_true.png')
        self.and_block_false=pygame.image.load('blocks/and_block_false.png')
        self.and3_block_true=pygame.image.load('blocks/and3_true.png')
        self.and3_block_false=pygame.image.load('blocks/and3_false.png')

        self.or_block_true=pygame.image.load('blocks/or_block_true.png')
        self.or_block_false=pygame.image.load('blocks/or_block_false.png')
        self.or3_block_true=pygame.image.load('blocks/or3_true.png')
        self.or3_block_false=pygame.image.load('blocks/or3_false.png')

        self.not_block_true=pygame.image.load('blocks/invertion_block_true.png')
        self.not_block_false=pygame.image.load('blocks/invertion_block_false.png')

