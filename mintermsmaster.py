#coding:utf-8
class Minterms_master:
    def __init__(self,minterms_string):
        self.minterms=minterms_string.split("v")
        self.splited_minterms=[]
        self.count_of_variables=1
        self.x_values=[]
        self.levels=1
        self.split_minterms()
        self.minterms_x_values=[]
        self.levels_values=[]


    def split_minterms(self):
        self.splited_minterms=[]
        for i in range(len(self.minterms)):
            self.minterms[i]=self.minterms[i][1:-1]
        for i in self.minterms:
            self.splited_minterms.append(i.split("^"))


    def set_x_values(self,x_values):

        self.x_values=[]
        for i in x_values:
            if i==1:
                self.x_values.append(True)
            elif i==0:
                self.x_values.append(False)




    def get_mainterms_x_values(self):
        self.minterms_x_values=[]
        for i in range(len(self.splited_minterms)):
            self.minterms_x_values.append([])
            for j in range(len(self.splited_minterms[i])):
                if self.splited_minterms[i][j][0]=='n':
                    x_index=int(self.splited_minterms[i][j][-1])-1
                    x_value=self.x_values[x_index]
                    self.minterms_x_values[i].append(not x_value)
                else:
                    x_index = int(self.splited_minterms[i][j][-1]) - 1
                    x_value = self.x_values[x_index]
                    self.minterms_x_values[i].append(x_value)


    #получение индекса координаты y на шине для текущей x переменной
    def get_ycor_index(self, value):
        x_count=self.count_of_variables*2
        if value[0]=='n':
            index=int(value[-1])-1
        else:
            index = int(value[-1])- 1+self.count_of_variables
        return index

    def get_distane(self):
        distance=5
        for i in self.splited_minterms:
            for j in i:
                distance+=10
        if distance>120:
            return distance+30
        return 120


    def get_height_canvas(self):
        height=300
        for i in self.splited_minterms:
            for j in i:
                height+=50
        return height+len(self.x_values)*100


    def ors_splitter(self,blocks):
        splitted_block=[]
        counter=0
        for i in range(0,len(blocks),3):
            splitted_block.append([])
            if len(blocks)-i >= 3:
                splitted_block[counter].append(blocks[i])
                splitted_block[counter].append(blocks[i+1])
                splitted_block[counter].append(blocks[i+2])
                counter+=1
            else:
                for j in range(i,len(blocks)):
                    splitted_block[counter].append(blocks[j])
        return splitted_block




