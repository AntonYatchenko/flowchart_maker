# coding:utf-8
import pygame

from blocks import Blocks
from blocks_on_drawplace import And_on_canvas, And3_on_canvas, Or_on_canvas, Or3_on_canvas, X_to_or
from colors import Colors
from human import Human
from mintermsmaster import Minterms_master
from top_panel import Top_panel

pygame.init()

draw_scheme=False
logic_input=False
loading=False
count_x_input=False

font = pygame.font.SysFont('Times New Roman', 11,False,False)
logic_font= pygame.font.SysFont('Tahoma', 18,False,False)

boolean_b = '(nx4^nx5)v(x1^nx2^nx3^nx4)v(x2^x3^x4)v(x1^x2^nx5)v(x1^x2^x3)v(x1^x3^nx5)'.replace(" ", "")
# boolean_b = '(x1)v(x2)'.replace(" ", "")
# boolean_b='(nx4^nx2^x1^x3^nx5)v(nx1^nx3)v(x3^x4^nx4^x2)v(x2^x3^nx1)v(x4)'.replace(" ","")
# boolean_b='(nx4^nx2^x1^x3^nx5)v(x2)'.replace(" ","")

count_x=5

blocks = Blocks()
human = Human()
panel=Top_panel()

master = Minterms_master(boolean_b)

canvas_size = [1320,master.get_height_canvas()]

window = pygame.display.set_mode((1320, 600))
top_panel = pygame.Surface((1320,100))

pygame.display.set_caption("Flowchart Maker")
drawplace = pygame.Surface(canvas_size)
drawplace.fill((99, 158, 173))

colors = Colors()
clock = pygame.time.Clock()

game_on = True
y_canvas = 100
x = 0
panel.create_xvalues(count_x)
pygame.key.set_repeat(1000, 1)


def connect(point1, point2, point3, point4):
    global linestep, current_level_xcor, drawplace, colors

    pygame.draw.line(drawplace, colors.black, point1, point2, 1)
    pygame.draw.line(drawplace, colors.black, point2, point3, 1)
    pygame.draw.line(drawplace, colors.black, point3, point4, 1)

def draw_and2_block(drawplace, x, y, boolean, xfrom, yfrom):
    global y_draw, linestep
    current_block = And_on_canvas(x, y, boolean)

    current_block.place(drawplace)

    for j in range(2):
        point1 = (xfrom[j], yfrom[j])
        point2 = (xfrom[j] + linestep, yfrom[j])
        point3 = (xfrom[j] + linestep, current_block.y_ins[j])
        point4 = (current_block.xleft, current_block.y_ins[j])

        connect(point1, point2, point3, point4)

        linestep += 10

    y_draw += 120
    return current_block

def draw_and3_block(drawplace, x, y, boolean, xfrom, yfrom):
    global y_draw, linestep
    current_block = And3_on_canvas(x, y, boolean)

    current_block.place(drawplace)

    for j in range(3):
        point1 = (xfrom[j], yfrom[j])
        point2 = (xfrom[j] + linestep, yfrom[j])
        point3 = (xfrom[j] + linestep, current_block.y_ins[j])
        point4 = (current_block.xleft, current_block.y_ins[j])

        connect(point1, point2, point3, point4)

        linestep += 10

    y_draw += 150
    return current_block

def draw_or2_block(drawplace, x, y, boolean, xfrom, yfrom):
    global y_draw, linestep
    current_block = Or_on_canvas(x, y, boolean)

    current_block.place(drawplace)

    for j in range(2):
        point1 = (xfrom[j], yfrom[j])
        point2 = (xfrom[j] + linestep, yfrom[j])
        point3 = (xfrom[j] + linestep, current_block.y_ins[j])
        point4 = (current_block.xleft, current_block.y_ins[j])

        connect(point1, point2, point3, point4)

        linestep += 10

    y_draw += 120
    return current_block

def draw_or3_block(drawplace, x, y, boolean, xfrom, yfrom):
    global y_draw, linestep
    current_block = Or3_on_canvas(x, y, boolean)

    current_block.place(drawplace)


    for j in range(3):
        point1 = (xfrom[j], yfrom[j])
        point2 = (xfrom[j] + linestep, yfrom[j])
        point3 = (xfrom[j] + linestep, current_block.y_ins[j])
        point4 = (current_block.xleft, current_block.y_ins[j])

        connect(point1, point2, point3, point4)

        linestep += 10

    y_draw += 150
    return current_block

while game_on:
    clock.tick(10)
    over_level = False
    mouse_pos=pygame.mouse.get_pos()
    mouse_x,mouse_y=mouse_pos[0],mouse_pos[1]

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            game_on = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_DOWN:
                y_canvas -= 100
                if y_canvas < 600 - canvas_size[1]:
                    y_canvas += 100

            if e.key == pygame.K_UP:
                y_canvas += 100
                if y_canvas >= 100:
                    y_canvas = 100

            if e.key == pygame.K_LEFT:
                y_canvas = 100

            if e.key == pygame.K_RIGHT:
                y_canvas = 600 - canvas_size[1]

            if e.key == pygame.K_SPACE and not draw_scheme:
                draw_scheme =True
                loading=False



            if logic_input:
                if e.key == pygame.K_1:
                    boolean_b += "1"
                if e.key == pygame.K_2:
                    boolean_b += "2"

                if e.key == pygame.K_3:
                    boolean_b += "3"

                if e.key == pygame.K_4:
                    boolean_b += "4"

                if e.key == pygame.K_5:
                    boolean_b += "5"

                if e.key == pygame.K_9:
                    boolean_b += "("

                if e.key == pygame.K_0:
                    boolean_b += ")"

                if e.key == pygame.K_v:
                    boolean_b += "v"

                if e.key == pygame.K_6:
                    boolean_b += "^"


                if e.key == pygame.K_x:
                    boolean_b += "x"

                if e.key == pygame.K_n:
                    boolean_b += "n"

                if e.key == pygame.K_e:
                    boolean_b += "e"

                if e.key == pygame.K_BACKSPACE:
                    boolean_b = boolean_b[0:len(boolean_b) - 1]


            if count_x_input :
                if e.key == pygame.K_1:
                    count_x = 1

                if e.key == pygame.K_2:
                    count_x = 2

                if e.key == pygame.K_3:
                    count_x = 3

                if e.key == pygame.K_4:
                    count_x = 4

                if e.key == pygame.K_5:
                    count_x = 5

                panel.create_xvalues(count_x)
                count_x_input=False


        if e.type == pygame.MOUSEBUTTONDOWN:
            if e.button==1 and mouse_x>=135 and mouse_y>=50 and mouse_x<=844 and mouse_y<=85:
                logic_input=True
                count_x_input=False


            elif e.button==1 and mouse_x>=873 and mouse_y>=50 and mouse_x<=940 and mouse_y<=85:
                count_x_input=True
                logic_input=False

            else:
                count_x_input=False
                logic_input=False

            if e.button == 1 and mouse_x >= 1024 and mouse_y >= 50 and mouse_x <= 1024+35 and mouse_y <= 85:
                    panel.change_xvalue(0)

            if e.button == 1 and mouse_x >= 1024+53 and mouse_y >= 50 and mouse_x <= 1024+35+53 and mouse_y <= 85 and count_x>=2:
                    panel.change_xvalue(1)

            if e.button == 1 and mouse_x >= 1024+53*2 and mouse_y >= 50 and mouse_x <= 1024+53*2+35 and mouse_y <= 85 and count_x>=3:
                    panel.change_xvalue(2)

            if e.button == 1 and mouse_x >= 1024+53*3 and mouse_y >= 50 and mouse_x <= 1024+53*3+35 and mouse_y <= 85 and count_x>=4:
                    panel.change_xvalue(3)

            if e.button == 1 and mouse_x >= 1024+53*4 and mouse_y >= 50 and mouse_x <= 1024+53*4+35 and mouse_y <= 85 and count_x>=5:
                    panel.change_xvalue(4)

                ############################################################################################

    top_panel.blit(pygame.image.load('top_panel/top_fon.png'), (0, 0))
    if logic_input:
        current_color=colors.red
    else:
        current_color=colors.black
    rendered_logic_str = logic_font.render(boolean_b, True, current_color)
    top_panel.blit(rendered_logic_str, (135, 53))

    if count_x_input:
        current_color = colors.red
    else:
        current_color = colors.black

    rendered_count_x=logic_font.render(str(count_x), True, current_color)
    top_panel.blit(rendered_count_x, (900, 53))

    panel.x_indication(top_panel)

    ###############   отрисовка    ##############################################################
    x_draw = 10
    y_draw = 10
    current_level_ycors = []

    window.fill((0, 0, 0))



    if draw_scheme and not loading:


        # отрисовка шин переменных###############################################################################################
        master.set_x_values(panel.x_values)
        master.minterms=boolean_b.split("v")
        master.split_minterms()
        master.get_mainterms_x_values()
        master.count_of_variables = count_x
        drawplace.fill((99, 158, 173))
        curent_level = 0
        for i in range(master.count_of_variables):
            if master.x_values[i]:
                current_color = colors.green
            else:
                current_color = colors.red

            x_text=font.render("x%d"%(i+1),True,current_color)
            pygame.draw.line(drawplace, current_color, (x_draw, 0), (x_draw, canvas_size[1]), 1)
            drawplace.blit(x_text,(x_draw+2,20))
            if not master.x_values[i]:
                drawplace.blit(blocks.not_block_true, (x_draw + 90 - 15 * i, y_draw))
            else:
                drawplace.blit(blocks.not_block_false, (x_draw + 90 - 15 * i, y_draw))
            pygame.draw.line(drawplace, current_color, (x_draw, y_draw + 50), (x_draw + 90 - 15 * i, y_draw + 50), 1)
            current_level_ycors.append(y_draw + 50)

            x_draw += 15
            y_draw += 120

        x_draw = 10
        y_draw -= 30

        for i in range(master.count_of_variables):
            if master.x_values[i]:
                current_color = colors.green
            else:
                current_color = colors.red
            pygame.draw.line(drawplace, current_color, (x_draw, y_draw + 50), (x_draw + 156 - 15 * i, y_draw + 50), 1)
            current_level_ycors.append(y_draw + 50)
            x_draw += 15
            y_draw += 27
            current_level_xcor = x_draw + 156 - 15 * i - 15

        y_draw = 10

        blocks_and_level = []

        # Отрисовка второго уровня###################################################################################
        linestep = 10
        distance_btwn_level = master.get_distane()
        for i in range(len(master.splited_minterms)):  # уровень И
            if len(master.splited_minterms[i]) == 1:
                boolean=master.minterms_x_values[0]
                xcor_x=current_level_xcor
                y_1st=current_level_ycors[master.get_ycor_index(master.splited_minterms[i][0])]
                current_block=X_to_or(xcor_x,y_1st,boolean)
                blocks_and_level.append(current_block)


            if len(master.splited_minterms[i]) == 2:  # для двух переменных
                boolean = master.minterms_x_values[i][0] and master.minterms_x_values[i][1]
                y_1st = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][0])]
                y_2nd = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][1])]
                x_block = current_level_xcor + distance_btwn_level
                current_block = draw_and2_block(drawplace, x_block, y_draw, boolean,
                                                (current_level_xcor, current_level_xcor), (y_1st, y_2nd))
                blocks_and_level.append(current_block)

            if len(master.splited_minterms[i]) == 3:
                and_value = master.minterms_x_values[i][0] and master.minterms_x_values[i][1] and \
                            master.minterms_x_values[i][2]
                current_block = And3_on_canvas(current_level_xcor + distance_btwn_level, y_draw, and_value)
                blocks_and_level.append(current_block)
                current_block.place(drawplace)

                for j in range(3):
                    ycor_x = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][j])]

                    point1 = (current_level_xcor, ycor_x)
                    point2 = (current_level_xcor + linestep, ycor_x)
                    point3 = (current_level_xcor + linestep, current_block.y_ins[j])
                    point4 = (current_block.xleft, current_block.y_ins[j])

                    connect(point1, point2, point3, point4)

                    linestep += 10

                y_draw += 150

            if len(master.splited_minterms[i]) == 4:
                double_blocks = []
                for j in range(0, 4, 2):
                    boolean = boolean = master.minterms_x_values[i][j] and master.minterms_x_values[i][j + 1]
                    y_1st = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][j])]
                    y_2nd = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][j + 1])]
                    x_block = current_level_xcor + distance_btwn_level
                    current_block = draw_and2_block(drawplace, x_block, y_draw, boolean,
                                                    (current_level_xcor, current_level_xcor), (y_1st, y_2nd))
                    double_blocks.append(current_block)
                boolean = double_blocks[0].value and double_blocks[1].value
                xsfrom = (double_blocks[0].xright, double_blocks[1].xright)
                ysfrom = (double_blocks[0].y_out, double_blocks[1].y_out)
                value = linestep
                linestep = (i + 1) * 5
                current_block = draw_and2_block(drawplace, current_block.xright + 50,
                                                double_blocks[1].y_corner - 10, boolean, xsfrom, ysfrom)
                blocks_and_level.append(current_block)
                linestep = value
                y_draw -= 120
                over_level = True

            if len(master.splited_minterms[i]) == 5:
                double_blocks = []
                # двухвходовый блок И
                boolean = boolean = master.minterms_x_values[i][0] and master.minterms_x_values[i][1]
                y_1st = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][0])]
                y_2nd = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][1])]
                x_block = current_level_xcor + distance_btwn_level
                current_block = draw_and2_block(drawplace, x_block, y_draw, boolean,
                                                (current_level_xcor, current_level_xcor), (y_1st, y_2nd))
                double_blocks.append(current_block)
                # трехвходовый блок И
                boolean = boolean = master.minterms_x_values[i][2] and master.minterms_x_values[i][3] and \
                                    master.minterms_x_values[i][4]
                y_1st = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][2])]
                y_2nd = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][3])]
                y_3rd = current_level_ycors[master.get_ycor_index(master.splited_minterms[i][4])]
                x_block = current_level_xcor + distance_btwn_level
                current_block = draw_and3_block(drawplace, x_block, y_draw, boolean,
                                                (current_level_xcor, current_level_xcor, current_level_xcor),
                                                (y_1st, y_2nd, y_3rd))
                double_blocks.append(current_block)

                boolean = double_blocks[0].value and double_blocks[1].value
                xsfrom = (double_blocks[0].xright, double_blocks[1].xright)
                ysfrom = (double_blocks[0].y_out, double_blocks[1].y_out)
                value = linestep
                linestep = (i + 1) * 5
                current_block = draw_and2_block(drawplace, current_block.xright + 50,
                                                double_blocks[1].y_corner - 10, boolean, xsfrom, ysfrom)
                blocks_and_level.append(current_block)
                linestep = value
                y_draw -= 120
                over_level = True

        if over_level:
            current_level_xcor = x_block + distance_btwn_level  + 66
        else:
            current_level_xcor += distance_btwn_level + 66
        y_draw = 10
        linestep = 10

        #########################################################################################################
        # Уровень ИЛИ
        current_or_blocks = master.ors_splitter(blocks_and_level)
        final_or_blocks = []
        next_or_blocks = []
        final_or = False

        while True:
            for i in range(len(current_or_blocks)):
                if len(current_or_blocks[i]) == 1:
                    final_or_blocks.append(current_or_blocks[i][0])

                if len(current_or_blocks[i]) == 2:
                    or_value = current_or_blocks[i][0].value or current_or_blocks[i][1].value
                    current_block = Or_on_canvas(current_level_xcor + distance_btwn_level /2, y_draw, or_value)
                    next_or_blocks.append(current_block)
                    current_block.place(drawplace)

                    for j in range(2):
                        ycor_x = current_or_blocks[i][j].y_out

                        point1 = (current_or_blocks[i][j].xright, ycor_x)
                        point2 = (current_level_xcor + linestep, ycor_x)
                        point3 = (current_level_xcor + linestep, current_block.y_ins[j])
                        point4 = (current_block.xleft, current_block.y_ins[j])

                        connect(point1, point2, point3, point4)

                        linestep += 10

                    y_draw += 120

                if len(current_or_blocks[i]) == 3:
                    or_value = current_or_blocks[i][0].value or current_or_blocks[i][1].value or current_or_blocks[i][
                        2].value
                    current_block = Or3_on_canvas(current_level_xcor + distance_btwn_level / 2, y_draw, or_value)
                    next_or_blocks.append(current_block)
                    current_block.place(drawplace)

                    for j in range(3):
                        ycor_x = current_or_blocks[i][j].y_out

                        point1 = (current_or_blocks[i][j].xright, ycor_x)
                        point2 = (current_level_xcor + linestep, ycor_x)
                        point3 = (current_level_xcor + linestep, current_block.y_ins[j])
                        point4 = (current_block.xleft, current_block.y_ins[j])

                        connect(point1, point2, point3, point4)

                        linestep += 10

                    y_draw += 150

            if final_or:
                break

            if len(next_or_blocks) + len(final_or_blocks) > 3:
                current_or_blocks = master.ors_splitter(next_or_blocks)
                next_or_blocks = []
                current_level_xcor += distance_btwn_level

            else:
                for k in range(len(next_or_blocks)):
                    final_or_blocks.append(next_or_blocks[k])
                current_or_blocks = master.ors_splitter(final_or_blocks)
                next_or_blocks = []
                current_level_xcor += distance_btwn_level
                final_or = True

            y_draw = 10
        pygame.draw.line(drawplace, colors.black, (current_block.xright, current_block.y_out),
                         (current_block.xright + 100, current_block.y_out), 1)
        #########################################################################################################
        loading=True
        draw_scheme=False


    # for i in blocks_and_level:
    #     pygame.draw.line(drawplace,colors.black,(i.xright,i.y_out),(current_level_xcor,i.y_out),1)


    

    window.blit(drawplace, (x, y_canvas))

    human.move(window)

    window.blit(top_panel, (0, 0))


    pygame.display.flip()

pygame.quit()