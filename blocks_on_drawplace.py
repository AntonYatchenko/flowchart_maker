from blocks import Blocks


class And_on_canvas:
    def __init__(self,x,y,boolean_value):
        self.y_corner=y
        self.xleft=x
        self.xright=x+66
        self.y_ins=[y+33,y+33*2]
        self.y_out=y+50
        self.block=self.block_value(boolean_value)
        self.value=boolean_value

    def place(self,drawplace):
        drawplace.blit(self.block,(self.xleft,self.y_corner))

    def block_value(self,boolean_value):
        blocks=Blocks()
        if boolean_value:
            return blocks.and_block_true
        return blocks.and_block_false

class And3_on_canvas:
    def __init__(self,x,y,boolean_value):
        self.y_corner=y
        self.xleft=x
        self.xright=x+66
        self.y_ins=[y+33,y+33*2,y+33*3]
        self.y_out=y+66
        self.block=self.block_value(boolean_value)
        self.value = boolean_value

    def place(self,drawplace):
        drawplace.blit(self.block,(self.xleft,self.y_corner))

    def block_value(self,boolean_value):
        blocks=Blocks()
        if boolean_value:
            return blocks.and3_block_true
        return blocks.and3_block_false

class Or_on_canvas:
    def __init__(self,x,y,boolean_value):
        self.y_corner=y
        self.xleft=x
        self.xright=x+66
        self.y_ins=[y+33,y+33*2]
        self.y_out=y+50
        self.block=self.block_value(boolean_value)
        self.value=boolean_value

    def place(self,drawplace):
        drawplace.blit(self.block,(self.xleft,self.y_corner))

    def block_value(self,boolean_value):
        blocks=Blocks()
        if boolean_value:
            return blocks.or_block_true
        return blocks.or_block_false

class Or3_on_canvas:
    def __init__(self,x,y,boolean_value):
        self.y_corner=y
        self.xleft=x
        self.xright=x+66
        self.y_ins=[y+33,y+33*2,y+33*3]
        self.y_out=y+66
        self.block=self.block_value(boolean_value)
        self.value = boolean_value

    def place(self,drawplace):
        drawplace.blit(self.block,(self.xleft,self.y_corner))

    def block_value(self,boolean_value):
        blocks=Blocks()
        if boolean_value:
            return blocks.or3_block_true
        return blocks.or3_block_false


class X_to_or:
    def __init__(self,x,y,boolean_value):
        self.xright=x
        self.y_out=y
        self.value=boolean_value