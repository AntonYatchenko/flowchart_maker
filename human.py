import pygame


class Human:
    def __init__(self):
        self.xcor=0
        self.ycor=600-150
        self.move_right=True
        self.moves=self.pictures()
        self.current_pic=0
        self.move_counter=0
        self.stand=False
        self.stand_counter=0
        self.speaks=self.get_speaks()
        self.index_speak=0

    def move(self,window):
        if self.move_right and not self.stand:
            window.blit(self.moves[self.move_counter], (self.xcor, self.ycor))

            self.xcor+=30
            self.move_counter+=1
            if self.move_counter>6:
                self.move_counter=1
            if self.xcor>1320-100:
                self.move_right=False
                self.move_counter=7
                self.stand=True

        elif not self.move_right and not self.stand:
            window.blit(self.moves[self.move_counter], (self.xcor, self.ycor))
            self.xcor-=30
            self.move_counter+=1
            if self.move_counter>12:
                self.move_counter=7
            if self.xcor<0:
                self.move_right=True
                self.move_counter=1

        else:
            self.stand_speak(window)

    def stand_speak(self,window):
        window.blit(self.moves[-1], (self.xcor, self.ycor))
        if self.stand_counter<600:
            window.blit(self.speaks[self.index_speak], (self.xcor-120, self.ycor-70))

            if self.stand_counter > 600/(len(self.speaks))*(self.index_speak+1):
                self.index_speak+=1
        else:
            self.stand=False
            self.stand_counter=0
            self.index_speak=0
        self.stand_counter+=1



    def pictures(self):
        pics=[]
        pics.append(pygame.image.load('human\human_right_stand.png'))

        pics.append(pygame.image.load('human\human_right_1.png'))
        pics.append(pygame.image.load('human\human_right_2.png'))
        pics.append(pygame.image.load('human\human_right_3.png'))
        pics.append(pygame.image.load('human\human_right_4.png'))
        pics.append(pygame.image.load('human\human_right_5.png'))
        pics.append(pygame.image.load('human\human_right_6.png'))

        pics.append(pygame.image.load('human\human_left_1.png'))
        pics.append(pygame.image.load('human\human_left_2.png'))
        pics.append(pygame.image.load('human\human_left_3.png'))
        pics.append(pygame.image.load('human\human_left_4.png'))
        pics.append(pygame.image.load('human\human_left_5.png'))
        pics.append(pygame.image.load('human\human_left_6.png'))

        pics.append(pygame.image.load('human\human_left_stand.png'))
        return pics

    def get_speaks(self):
        speaks=[]
        speaks.append(pygame.image.load('human\speak_1.png'))
        speaks.append(pygame.image.load('human\speak_2.png'))
        speaks.append(pygame.image.load('human\speak_3.png'))
        speaks.append(pygame.image.load('human\speak_4.png'))
        speaks.append(pygame.image.load('human\speak_5.png'))
        speaks.append(pygame.image.load('human\speak_6.png'))
        speaks.append(pygame.image.load('human\speak_7.png'))
        speaks.append(pygame.image.load('human\speak_8.png'))
        speaks.append(pygame.image.load('human\speak_9.png'))
        speaks.append(pygame.image.load('human\speak_10.png'))
        speaks.append(pygame.image.load('human\speak_11.png'))
        speaks.append(pygame.image.load('human\speak_12.png'))

        return speaks